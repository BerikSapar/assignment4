import java.util.ArrayList;

public class Group {
    private ArrayList<User> users;
    private int id;
    private String name;

    public Group(int id, String name){
        this.name = name;
        this.id = id;
    }

    public void addUser(User user){
        users.add(new User(user));
    }
}
