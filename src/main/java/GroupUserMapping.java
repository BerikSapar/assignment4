import java.util.ArrayList;
import java.io.*;
import java.lang.*;
import java.lang.Integer.*;
import java.util.*;

public class GroupUserMapping {
	private String mappingFilename, groupsFilename;
	private HashMap<Integer, HashMap<Integer, HashSet<Integer>>> map;
	private ArrayList<Group> groups;
	public GroupUserMapping(){
		map = new HashMap<Integer, HashMap<Integer, HashSet<Integer>>>();
		groups = new ArrayList<Group>();
	}
	public GroupUserMapping(String mappingFilename, String groupsFilename){
		this();
		this.mappingFilename = mappingFilename;
		this.groupsFilename = groupsFilename;
	}
	public ArrayList<Integer> getUsers(int group_id){
		ArrayList<Integer> users = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, HashMap<Integer, HashSet<Integer>>> cursor : map.entrySet()){
			for (HashMap.Entry<Integer, HashSet<Integer>> group : cursor.getValue().entrySet()){
				if ((int)(group.getKey()) == group_id)
					for (int user : group.getValue())
						users.add(user);
			}
		}
		return users;
	}
	public void load(){
		load(mappingFilename, groupsFilename);
	}
	public void load(String mappingFilename, String groupsFilename){
		map.clear();
		this.mappingFilename = mappingFilename;
		this.groupsFilename = groupsFilename;
		try {
			Scanner in = new Scanner(new File(mappingFilename));
			while (in.hasNextLine()){
				String line = in.nextLine();
				String[] s = line.split(" ");
				if (s.length != 3)
					throw new Exception("File " + mappingFilename + " is not properly structured");
				try {
					int id = Integer.parseInt(s[0]);
					int group_id = Integer.parseInt(s[1]);
					int user_id = Integer.parseInt(s[2]);
					if (!map.containsKey(id))
						map.put(id, new HashMap<Integer, HashSet<Integer>>());
					if (!map.get(id).containsKey(group_id))
						map.get(id).put(group_id, new HashSet<Integer>());
					map.get(id).get(group_id).add(user_id);
				} catch (NumberFormatException e){
					System.out.println("File " + mappingFilename + " must contain only numbers");
				}
			}
		} catch (FileNotFoundException e){
			System.out.println("File not found: " + mappingFilename);
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		try {
			Scanner in = new Scanner(new File(groupsFilename));
			while (in.hasNextLine()){
				String line = in.nextLine();
				String[] s = line.split(" ");
				if (s.length != 2)
					throw new Exception("File " + groupsFilename + " is not properly structured");
				try {
					int id = Integer.parseInt(s[0]);
					String name = s[1];
					groups.add(new Group(id, name));
				} catch (NumberFormatException e){
					System.out.println("File " + groupsFilename + " first must be integer id");
				}
			}
		} catch (FileNotFoundException e){
			System.out.println("File not found: " + groupsFilename);
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		// for (HashMap.Entry<Integer, HashMap<Integer, HashSet<Integer>>> cursor : map.entrySet()){
		// 	System.out.print("key: " + cursor.getKey() + ", ");
		// 	for (HashMap.Entry<Integer, HashSet<Integer>> group : cursor.getValue().entrySet()){
		// 		System.out.print("group_id: " + group.getKey() + ", ");
		// 		for (int user_id : group.getValue())
		// 			System.out.print(", user_id: " + user_id);
		// 	}
		// 	System.out.println();
		// }
	}
}