import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private ArrayList<User> userList;
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private String dbFilename;
    private GroupUserMapping groupUserMapping;

    public Main(String dbFilename){
        this.dbFilename = dbFilename;
        groupUserMapping = new GroupUserMapping("groups_user_mapping.txt", "groups.txt");
        groupUserMapping.load();
    }

    public static void main(String[] args){
        Main main = new Main("db.txt");
        main.start();
    }

    private void addUser(User user) {
        userList.add(user);
        register(user);
    }

    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Sign In");
                System.out.println("2. Sign Up");
                System.out.println("3. Show users by group");
                System.out.println("4. Exit");
                int choice = sc.nextInt();
                if (choice == 1) signIn();
                else if (choice == 2) signUp();
                else if (choice == 3) showUsers();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void userProfile() {
        signedUser = null;
    }

    private void logOff() {

    }

    private void authentication(String login, String password) {
        try {
            Scanner in = new Scanner(new File(dbFilename));
            while (in.hasNextLine()) {
                String s = in.nextLine();
                String[] sArray = s.split(" ");

                if (login.equals(sArray[3]) && password.equals(sArray[4])){
                    System.out.println("Successfully logged in");
                    System.exit(0);
                }
            }
            System.out.println("User not found");

            in.close();

        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void register(User user) {
        try {
            FileWriter write = new FileWriter(dbFilename, true);
            BufferedWriter writer = new BufferedWriter(write);
            writer.write(user.toString());
            writer.close();
            System.out.println("Successfully registered");
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void signIn() {
        System.out.println("----Sign In----");
        System.out.println("Login:");
        Scanner in = new Scanner(System.in);
        String login = in.nextLine();
        System.out.println("Password:");
        String password = in.nextLine();
        authentication(login, password);
    }

    private void signUp() {
        System.out.println("----Sign Up----");
        System.out.println("Login:");
        Scanner in = new Scanner(System.in);
        String username = in.nextLine();
        System.out.println("Password:");
        String password = in.nextLine();
        System.out.println("Name:");
        String name = in.nextLine();
        System.out.println("Surname:");
        String surname = in.nextLine();

        int id = 1;
        boolean exists = true;
        while (exists){
            exists = false;
            for (int i = 0; i < userList.size(); i++) {
                if (userList.get(i).getId() == id) {
                    exists = true;
                }
            }
            if (exists)
                id++;
        }
        User user = new User(id, name, surname, username, password);

        addUser(user);
    }

    private void showUsers(){
        System.out.println("Enter group_id:");
        Scanner in = new Scanner(System.in);
        int group_id = in.nextInt();
        ArrayList<Integer> users = groupUserMapping.getUsers(group_id);
        for (int i : users){
            for (User user : userList){
                if (user.getId() == i){
                    System.out.println("Username: " + user.getUsername() + ", name: " + user.getName() + ", surname: " + user.getSurname());
                }
            }
        }
    }

    public void start() {
        userList = new ArrayList<User>();
        try {
            Scanner in = new Scanner(new File(dbFilename));
            while (in.hasNextLine()){
                String line = in.nextLine();
                String [] words = line.split(" ");
                int id = Integer.parseInt(words[0]);
                String name = words[1];
                String surname = words[2];
                String username = words[3];
                String password = words[4];
                userList.add(new User(id, name, surname, username, password));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }
    }

    private void saveUserList() {

    }
}
