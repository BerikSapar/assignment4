public class User extends Password {
    private  int id;
    private String name;
    private String surname;
    private String username;
    private String password;

    public User(int id,String name, String surname, String username, String password) {
        this.id=id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public User(User another){
        this.id = another.id;
        this.name = another.name;
        this.surname = another.surname;
        this.username = another.username;
        this.password = another.password;
    }

    public int getId() {
        return id;
    }
 public void setId(int id) {
     this.id = id;
 }


    public String getName()
    {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String getPasswordStr() {
        return super.getPasswordStr();
    }

    @Override
    public void setPasswordStr(String passwordStr) {
        this.password = passwordStr;
    }

    @Override
    public String toString() {
        return id+" "+name+" "+surname+" "+username+" "+password+"\n";
    }
}
